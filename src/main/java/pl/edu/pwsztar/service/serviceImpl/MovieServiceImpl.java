package pl.edu.pwsztar.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.*;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.TxtGenerator;
import pl.edu.pwsztar.domain.mapper.MovieListMapper;
import pl.edu.pwsztar.domain.mapper.MovieMapper;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.MovieService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<MovieDto>> movieListMapper;
    private final Converter<CreateMovieDto, Movie> movieMapper;
    private final Converter<Long, MovieCounterDto> movieCounterMapper;
    private final TxtGenerator txtGenerator;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository,
                            MovieListMapper movieListMapper,
                            MovieMapper movieMapper,
                            Converter<Long, MovieCounterDto> movieCounterMapper, TxtGenerator txtGenerator) {

        this.movieRepository = movieRepository;
        this.movieListMapper = movieListMapper;
        this.movieMapper = movieMapper;
        this.movieCounterMapper = movieCounterMapper;
        this.txtGenerator = txtGenerator;
    }

    @Override
    public List<MovieDto> findAll() {
        List<Movie> movies = movieRepository.findAll();
        return movieListMapper.convert(movies);
    }

    @Override
    public void createMovie(CreateMovieDto createMovieDto) {
        Movie movie = movieMapper.convert(createMovieDto);
        movieRepository.save(movie);
    }

    @Override
    public void deleteMovie(Long movieId) {
        Optional<Movie> movieOptional = movieRepository.findById(movieId);
        movieOptional.ifPresent(movieRepository::delete);
    }

    @Override
    public MovieCounterDto countMovies() {
        return movieCounterMapper.convert(movieRepository.count());
    }

    @Override
    public FinishedFileDto getFileToSend() throws IOException {
        FileDto fileDto = new FileDto(findAll());

        File file = txtGenerator.toTxt(fileDto);
        InputStream inputStream = new FileInputStream(file);

        return new FinishedFileDto.Builder().
                inputStreamResource(new InputStreamResource(inputStream)).
                length(file.length()).
                build();
    }
}

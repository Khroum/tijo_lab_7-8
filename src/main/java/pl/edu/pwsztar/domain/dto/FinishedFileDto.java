package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

public class FinishedFileDto {
    private InputStreamResource inputStreamResource;
    private Long length;

    public FinishedFileDto(){
    }

    private FinishedFileDto(Builder builder){
        this.inputStreamResource = builder.inputStreamResource;
        this.length = builder.length;
    }

    public InputStreamResource getInputStreamResource() {
        return inputStreamResource;
    }

    public Long getLength() {
        return length;
    }

    public static final class Builder {
        private InputStreamResource inputStreamResource;
        private Long length;

        public Builder() {
        }


        public Builder inputStreamResource(InputStreamResource inputStreamResource) {
            this.inputStreamResource = inputStreamResource;
            return this;
        }

        public Builder length(Long length) {
            this.length = length;
            return this;
        }

        public FinishedFileDto build() {
            return new FinishedFileDto(this);
        }
    }

}

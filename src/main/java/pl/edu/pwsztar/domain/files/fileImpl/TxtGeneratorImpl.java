package pl.edu.pwsztar.domain.files.fileImpl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.TxtGenerator;
import java.io.*;


@Service
public class TxtGeneratorImpl implements TxtGenerator {
    private final static String PREFIX = "tmp";
    private final static String SUFFIX = ".txt";
    private static final Logger LOGGER = LoggerFactory.getLogger(TxtGeneratorImpl.class);

    @Override
    public File toTxt (FileDto fileDto) throws IOException{
        File file = getGeneratedFile(fileDto);

        return file;
    }

    private File getGeneratedFile(FileDto fileDto) throws IOException {
        File file = File.createTempFile(PREFIX,SUFFIX);
        FileOutputStream fileOutputStream = new FileOutputStream(file);

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

        writeAllMoviesToFile(bufferedWriter,fileDto);

        closeFile(fileOutputStream, bufferedWriter);

        return file;
    }

    private void writeAllMoviesToFile(BufferedWriter bufferedWriter,FileDto fileDto) throws IOException {
        fileDto.getMovieList().
                stream().
                sorted((movie1, movie2) -> (movie1.getYear()-movie2.getYear())*-1).
                forEach(movie->writeMovieToFile(bufferedWriter,movie));
    }

    private void writeMovieToFile(BufferedWriter bufferedWriter, MovieDto movie) {
            try {
                bufferedWriter.write(movie.getYear() + " " + movie.getTitle());
                bufferedWriter.newLine();
            } catch (IOException e) {
               LOGGER.info("writing to file went wrong");
            }
    }

    private void closeFile(FileOutputStream fileOutputStream, BufferedWriter bufferedWriter) throws IOException {
        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}

package pl.edu.pwsztar.domain.files;


import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.File;
import java.io.IOException;



@FunctionalInterface
public interface TxtGenerator {

   File toTxt(FileDto fileDto) throws IOException;

}
